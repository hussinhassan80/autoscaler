package config

type Executor struct {
	MaximumTimeout      int
	ExecutionMaxRetries int
	Port                int `toml:"-"`
}
