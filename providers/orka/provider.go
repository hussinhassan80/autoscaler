package orka

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/ssh"

	globalConfig "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/dial"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/images"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/orka"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/runner"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/orka/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

const (
	Name = "orka"

	defaultHTTPTimeout = 60 * time.Second
	defaultBootTimeout = 120 * time.Second

	projectParticipationMessage = "\n" +
		"                                                                     \033[33m*.                  *.\033[0m\n" +
		"                                                                    \033[33m***                 ***\033[0m\n" +
		"┌───────────────────────────────────────────────────────────────── \033[33m*****\033[0m ───────────── \033[33m*****\033[0m ──────┐\n" +
		"│                                                                 \033[33m.******             *******\033[0m      │\n" +
		"│                                                                 \033[33m********\033[0m            \033[33m********\033[0m     │\n" +
		"│                                                                \033[33m,,,,,,,,,***********,,,,,,,,,\033[0m     │\n" +
		"│    This project is not a participant in the MacOS program.    \033[33m,,,,,,,,,,,*********,,,,,,,,,,,\033[0m    │\n" +
		"│                                                               \033[33m.,,,,,,,,,,,*******,,,,,,,,,,,,\033[0m    │\n" +
		"│    To register your interest, please visit                        \033[33m,,,,,,,,,*****,,,,,,,,,.\033[0m       │\n" +
		"│    https://gitlab.com/gitlab-com/macos-buildcloud-runners-beta       \033[33m,,,,,,,****,,,,,,\033[0m           │\n" +
		"│                                                                         \033[33m.,,,***,,,,\033[0m              │\n" +
		"│                                                                             \033[33m,*,.\033[0m                 │\n" +
		"│                                                                                                  │\n" +
		"└──────────────────────────────────────────────────────────────────────────────────────────────────┘\n\n"
)

type Provider struct {
	cfg config.Provider

	bootTimeout time.Duration
	vmTag       string

	logger logging.Logger
	client orka.Client

	allowedProjects allowedProjects
}

func New(cfg globalConfig.Global, logger logging.Logger) (providers.Provider, error) {
	err := cfg.Orka.Validate()
	if err != nil {
		return nil, fmt.Errorf("invalid Orka configuration: %w", err)
	}

	endpoint, err := url.Parse(cfg.Orka.Endpoint)
	if err != nil {
		return nil, fmt.Errorf("invalid Orka endpoint URL: %w", err)
	}

	httpTimeout := time.Duration(cfg.Orka.HTTPTimeout) * time.Second
	if httpTimeout == 0 {
		httpTimeout = defaultHTTPTimeout
	}

	bootTimeout := time.Duration(cfg.Orka.BootTimeout) * time.Second
	if bootTimeout == 0 {
		bootTimeout = defaultBootTimeout
	}

	for alias := range cfg.Orka.ImageAliases {
		cfg.Orka.AllowedImages = append(cfg.Orka.AllowedImages, alias)
	}

	var betaAllowedProjects allowedProjects
	if cfg.Orka.BetaAllowedProjectsPath != "" {
		betaAllowedProjects, err = loadAllowedProjects(cfg.Orka.BetaAllowedProjectsPath)
		if err != nil {
			return nil, fmt.Errorf("loading allowed projects: %w", err)
		}
	}

	return &Provider{
		cfg:         cfg.Orka,
		bootTimeout: bootTimeout,
		vmTag:       cfg.VMTag,

		allowedProjects: betaAllowedProjects,

		logger: logger,
		client: orka.New(
			endpoint,
			cfg.Orka.Token,
			&orka.Config{
				Logger:          logger,
				BackoffSettings: nil,
				Client: &http.Client{
					Timeout: httpTimeout,
				},
			},
		),
	}, nil
}

func (p *Provider) VMName(runnerData vm.NameRunnerData) string {
	jobID := strconv.FormatInt(runnerData.JobID, 10)

	// strip vmTag to stay under Orka's 38 chars limit
	lenTag := 38 - len("runner--") - len(jobID)
	vmTag := p.vmTag
	if len(vmTag) > lenTag {
		vmTag = vmTag[:lenTag]
	}

	parts := []string{
		"runner",
		vmTag,
		jobID,
	}

	return strings.ToLower(strings.Join(parts, "-"))
}

func (p *Provider) VMNameForRunner(runnerData vm.NameRunnerData) string {
	if p.cfg.DeployFromConfig {
		// When deploying from configuration, VMs have no name. Return an empty string to the runner
		return ""
	}

	return p.VMName(runnerData)
}

func (p *Provider) Get(ctx context.Context, name string) (vm.Instance, error) {
	return vm.Instance{}, fmt.Errorf("GetVM not implemented. Ensure the ProviderCache is configured")
}

func (p *Provider) Create(ctx context.Context, e executors.Executor, vmInst *vm.Instance) error {
	if err := p.validateCreate(); err != nil {
		return err
	}

	log := p.logger
	if !p.cfg.DeployFromConfig {
		log = log.WithField("vm-name", vmInst.Name)
	}

	image, desc, err := p.resolveImageName()
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	if image == "" { // this happens when baseImage == "" and user hasn't specified an image
		fmt.Println("Error: Please select an image for your job by setting the `image:` keyword in your .gitlab-ci.yml")
		return fmt.Errorf("image is empty")
	}

	var vmConfigName string
	if p.cfg.DeployFromConfig {
		fmt.Printf("Using image %s\n", desc)
		log.Infof("Using Virtual Machine config %s", desc)
		vmConfigName = image
	} else {
		// If not deploying from a config, we instead create the VM config dynamically, using the settings
		// given to the provider and image name.
		fmt.Printf("Using image %s with %d cores\n", desc, p.cfg.Cores)
		log.Infof("Creating Virtual Machine using image %s with %d cores", desc, p.cfg.Cores)
		err = p.client.CreateVM(ctx, vmInst.Name, fmt.Sprintf("%s.img", image), p.cfg.Cores)
		if err != nil {
			return fmt.Errorf("unable to create VM: %w", err)
		}

		vmConfigName = vmInst.Name
	}

	log.Infof("Deploying Virtual Machine")
	vm, err := p.client.DeployVM(ctx, vmConfigName)
	if err != nil {
		return fmt.Errorf("unable to deploy VM: %w", err)
	}

	vmInst.Orka.ID = vm.ID
	vmInst.Username = p.cfg.User
	vmInst.Password = p.cfg.Password
	vmInst.IPAddress = vm.Addr.String()

	fmt.Printf("Waiting for machine %s to boot...\n", vm.ID)
	log.WithFields(logrus.Fields{
		"timeout_s": p.bootTimeout.Seconds(),
		"ip":        vm.Addr,
		"id":        vm.ID,
	}).Infof("Waiting for machine to boot")
	err = waitForSSHReachable(ctx, log, vmInst, p.bootTimeout)
	if err != nil {
		return fmt.Errorf("VM failed to boot in time: %w", err)
	}

	log.WithField("id", vm.ID).Info("Virtual Machine created")

	script, privateKey, err := firstRunScript(vmInst.Password)
	if err != nil {
		return fmt.Errorf("generating first run script: %w", err)
	}

	err = e.Execute(ctx, *vmInst, []byte(script))
	if err != nil {
		return fmt.Errorf("executing first run script: %w", err)
	}

	// save new auth data into provider cache for future executions
	vmInst.PrivateSSHKey = privateKey
	vmInst.Password = ""

	return nil
}

func (p *Provider) Delete(ctx context.Context, vmInst vm.Instance) error {
	log := p.logger
	if !p.cfg.DeployFromConfig {
		log = log.WithField("vm-name", vmInst.Name)
	}

	// if no ID exists, it indicates that no VM was created, so will likely had previously encountered an error
	// whilst provisioning.
	if vmInst.Orka.ID == "" {
		log.Infof("Skipping deletion of VM as it wasn't created")
		return nil
	}

	log.WithField("id", vmInst.Orka.ID).Infof("Deleting VM")

	if p.cfg.DeployFromConfig {
		// DeleteVM deletes a VM instance
		err := p.client.DeleteVM(ctx, vmInst.Orka.ID)
		if err != nil {
			return fmt.Errorf("unable to delete VM: %w", err)
		}
	} else {
		// PurgeVM deletes both instance and dynamically created VM config
		err := p.client.PurgeVM(ctx, vmInst.Name)
		if err != nil {
			return fmt.Errorf("unable to delete VM: %w", err)
		}
	}

	return nil
}

func (p *Provider) resolveImageName() (string, string, error) {
	image, err := images.Get(p.cfg.BaseImage, p.cfg.AllowedImages)
	if err != nil {
		return "", "", err
	}

	if target, ok := p.cfg.ImageAliases[image]; ok {
		return target, fmt.Sprintf("%s (aliased to %q)", image, target), nil
	}

	return image, image, nil
}

func waitForSSHReachable(ctx context.Context, log logging.Logger, vmInst *vm.Instance, timeout time.Duration) error {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	sshConfig := &ssh.ClientConfig{
		User:            vmInst.Username,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Auth:            []ssh.AuthMethod{ssh.Password(vmInst.Password)},
		Timeout:         dial.DefaultSSHTimeout,
	}

	for {
		_, err := dial.SSHWithDeadline(ctx, "tcp", vmInst.IPAddress, sshConfig)
		if err == nil {
			break
		}

		log.WithError(err).Debugf("Dialing %q failed", vmInst.IPAddress)

		select {
		case <-time.After(1 * time.Second):
			continue
		case <-ctx.Done():
			return ctx.Err()
		}
	}

	return nil
}

func (p *Provider) validateCreate() error {
	if len(p.allowedProjects) == 0 {
		return nil
	}

	uri, err := url.Parse(runner.GetAdapter().ProjectURL())
	if err != nil {
		return fmt.Errorf("unable to parse project URL: %q", runner.GetAdapter().ProjectURL())
	}

	if !p.allowedProjects.Contains(uri.Path) {
		fmt.Println(projectParticipationMessage)
		return runner.NewBuildFailureError(fmt.Errorf("project not a beta participant"))
	}

	return nil
}
