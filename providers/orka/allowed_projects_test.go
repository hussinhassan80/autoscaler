package orka

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestAllowedProject(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	require.NoError(t, err)
	defer os.RemoveAll(dir)

	pathname := filepath.Join(dir, "allowed_projects")
	require.NoError(t, ioutil.WriteFile(pathname, []byte(`
		/gitlab-org                     # top-level project
		/org/project/subproject         # nested group
		/org/a/prefix_                  # prefix

		# just a comment
	`), 0777))

	allowed, err := loadAllowedProjects(pathname)
	require.NoError(t, err)

	assert.True(t, allowed.Contains("/gitlab-org"))
	assert.True(t, allowed.Contains("/gitlab-org/any/project"))
	assert.True(t, allowed.Contains("/org/a/prefix_"))

	assert.False(t, allowed.Contains("/org/a/prefix_hello"))
	assert.False(t, allowed.Contains("gitlab-org"))
	assert.False(t, allowed.Contains(""))
	assert.False(t, allowed.Contains("g"))
	assert.False(t, allowed.Contains("/g"))
	assert.False(t, allowed.Contains("/org"))
	assert.False(t, allowed.Contains("/org/a/prefix"))
	assert.False(t, allowed.Contains("/org/a/prefiy_hello"))
	assert.False(t, allowed.Contains("/org/a/prefiz_hello"))
	assert.False(t, allowed.Contains("/gitlab-com/any/project"))
}
