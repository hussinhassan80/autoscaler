package orka

import (
	"context"
	"net"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

var testVMAddress = &net.TCPAddr{IP: net.ParseIP(testVMIP), Port: 22}

func TestGetSSHEndpoint(t *testing.T) {
	vmStatusDeployedObj := vmStatusRespBody{
		VirtualMachineResources: []vmResource{
			{
				VirtualMachineName: testVMName,
				VMDeploymentStatus: "Deployed",
				Status: []vmResourceStatus{
					{
						VirtualMachineName: testVMName,
						NodeStatus:         "UP",
						VirtualMachineIP:   testVMIP,
						SSHPort:            "22",
						CPU:                6,
						Vcpu:               6,
						VMStatus:           "running",
					},
				},
			},
		},
	}

	type getVMTestCase struct {
		testCaseBase

		vmName       string
		expectedAddr net.Addr
	}

	testCases := map[string]getVMTestCase{
		"fetch vm is successful": {
			testCaseBase: testCaseBase{
				token:                   "token1",
				statusCode:              http.StatusOK,
				respBody:                vmStatusDeployedObj,
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointStatus, testVMName),
				expectedRequestBody:     nil,
				expectedErr:             nil,
			},
			vmName:       testVMName,
			expectedAddr: testVMAddress,
		},
		"with different token, IP and VM name": {
			testCaseBase: testCaseBase{
				token:      "token2",
				statusCode: http.StatusOK,
				respBody: vmStatusRespBody{
					VirtualMachineResources: []vmResource{
						{
							VirtualMachineName: "myorkavm2",
							VMDeploymentStatus: "Deployed",
							Status: []vmResourceStatus{
								{
									VirtualMachineName: "myorkavm2",
									NodeStatus:         "UP",
									VirtualMachineIP:   "10.221.188.4",
									SSHPort:            "22",
									CPU:                6,
									Vcpu:               6,
									VMStatus:           "running",
								},
							},
						},
					},
				},
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointStatus, "myorkavm2"),
				expectedRequestBody:     nil,
				expectedErr:             nil,
			},
			vmName:       "myorkavm2",
			expectedAddr: &net.TCPAddr{IP: net.ParseIP("10.221.188.4"), Port: 22},
		},
		"fetch non-deployed vm is not successful": {
			testCaseBase: testCaseBase{
				token:      "token1",
				statusCode: http.StatusOK,
				respBody: vmStatusRespBody{
					VirtualMachineResources: []vmResource{
						{
							VirtualMachineName: testVMName,
							VMDeploymentStatus: "Not Deployed",
							Status:             []vmResourceStatus{},
						},
					},
				},
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointStatus, testVMName),
				expectedRequestBody:     nil,
				expectedErr:             ErrVMNotFound,
			},
			vmName:       testVMName,
			expectedAddr: nil,
		},
		"fetch non-existent vm": {
			testCaseBase: testCaseBase{
				token:                   "token1",
				statusCode:              http.StatusOK,
				respBody:                vmStatusDeployedObj,
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointStatus, "non-existent"),
				expectedRequestBody:     nil,
				expectedErr:             ErrVMNotFound,
			},
			vmName: "non-existent",
		},
		"fetch vm is successful after retry on StatusServiceUnavailable": {
			testCaseBase: testCaseBase{
				token:                   "token1",
				statusCode:              http.StatusOK,
				respBody:                vmStatusDeployedObj,
				backoffSettings:         testBackoff,
				expectedRequestAttempts: 2,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointStatus, testVMName),
				expectedRequestBody:     nil,
				expectedErr:             nil,
			},
			vmName:       testVMName,
			expectedAddr: testVMAddress,
		},
		"with invalid IP": {
			testCaseBase: testCaseBase{
				token:      "token2",
				statusCode: http.StatusOK,
				respBody: vmStatusRespBody{
					VirtualMachineResources: []vmResource{
						{
							VirtualMachineName: "myorkavm2",
							VMDeploymentStatus: "Deployed",
							Status: []vmResourceStatus{
								{
									VirtualMachineName: "myorkavm2",
									NodeStatus:         "UP",
									VirtualMachineIP:   "foo",
									SSHPort:            "22",
									CPU:                6,
									Vcpu:               6,
									VMStatus:           "running",
								},
							},
						},
					},
				},
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointStatus, "myorkavm2"),
				expectedRequestBody:     nil,
				expectedErr:             NewInvalidResponseError(`invalid IP format in "foo"`, nil),
			},
			vmName:       "myorkavm2",
			expectedAddr: nil,
		},
		"with invalid port": {
			testCaseBase: testCaseBase{
				token:      "token2",
				statusCode: http.StatusOK,
				respBody: vmStatusRespBody{
					VirtualMachineResources: []vmResource{
						{
							VirtualMachineName: "myorkavm2",
							VMDeploymentStatus: "Deployed",
							Status: []vmResourceStatus{
								{
									VirtualMachineName: "myorkavm2",
									NodeStatus:         "UP",
									VirtualMachineIP:   "127.0.0.1",
									SSHPort:            "foo",
									CPU:                6,
									Vcpu:               6,
									VMStatus:           "running",
								},
							},
						},
					},
				},
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointStatus, "myorkavm2"),
				expectedRequestBody:     nil,
				expectedErr:             NewInvalidResponseError(`parse SSH port "foo": strconv.Atoi: parsing "foo": invalid syntax`, nil),
			},
			vmName:       "myorkavm2",
			expectedAddr: nil,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			runTestCase(t, http.MethodGet, &tc.testCaseBase, func(c *client) error {
				sshEndpoint, err := c.GetSSHEndpoint(context.Background(), tc.vmName)
				if err != nil {
					return err
				}

				assert.Equal(t, tc.expectedAddr.Network(), sshEndpoint.Network(), "VM address network mismatch")
				assert.Equal(t, tc.expectedAddr.String(), sshEndpoint.String(), "VM address ip mismatch")

				return nil
			})
		})
	}
}
