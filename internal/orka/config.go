package orka

import (
	"net/http"
	"time"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/backoff"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
)

type Config struct {
	Client          *http.Client
	BackoffSettings *backoff.Settings
	Logger          logging.Logger
}

var DefaultConfig = Config{
	Client: &http.Client{Timeout: 5 * time.Second},
	Logger: logging.New(),
	BackoffSettings: &backoff.Settings{
		InitialInterval:     3,
		RandomizationFactor: 0.1,
		Multiplier:          1.5,
		MaxInterval:         30,
		MaxElapsedTime:      300,
	},
}
