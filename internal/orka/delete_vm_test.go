package orka

import (
	"context"
	"net/http"
	"testing"
)

func TestDeleteVM(t *testing.T) {
	type deleteVMTestCase struct {
		testCaseBase

		id string
	}

	testCases := map[string]deleteVMTestCase{
		"delete vm is successful": {
			testCaseBase: testCaseBase{
				token:      "token1",
				statusCode: http.StatusOK,
				respBody: vmStatusRespBody{
					respBaseBody: respBaseBody{
						Message: "Successfully deleted VM",
					},
				},
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointDelete),
				expectedRequestBody:     getExpectedDeployRequestBody(testVMName),
				expectedErr:             nil,
			},
			id: testVMName,
		},
		"with name of non-existent VM": {
			testCaseBase: testCaseBase{
				token:      "token3",
				statusCode: http.StatusBadRequest,
				respBody: vmStatusRespBody{
					respBaseBody: respBaseBody{
						Errors: respBodyErrors{
							{
								Message: "No VMs with that Name exist in Orka",
							},
							{
								Message: "Some other error",
							},
						},
					},
				},
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointDelete),
				expectedRequestBody:     getExpectedDeployRequestBody("non-existent"),
				expectedErr: &HTTPError{
					Endpoint:   getVMEndpointRelURL(endpointDelete),
					StatusCode: http.StatusBadRequest,
					Message:    "",
					Errors:     []string{"No VMs with that Name exist in Orka", "Some other error"},
				},
			},
			id: "non-existent",
		},
		"delete vm is successful after retry on StatusServiceUnavailable": {
			testCaseBase: testCaseBase{
				token:      "token1",
				statusCode: http.StatusOK,
				respBody: vmStatusRespBody{
					respBaseBody: respBaseBody{
						Message: "Successfully deleted VM",
					},
				},
				backoffSettings:         testBackoff,
				expectedRequestAttempts: 2,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointDelete),
				expectedRequestBody:     getExpectedDeployRequestBody(testVMName),
				expectedErr:             nil,
			},
			id: testVMName,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			runTestCase(t, http.MethodDelete, &tc.testCaseBase,
				func(c *client) error {
					return c.DeleteVM(context.Background(), tc.id)
				})
		})
	}
}
