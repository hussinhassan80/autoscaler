package orka

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateVM(t *testing.T) {
	respBody201 := vmStatusRespBody{
		respBaseBody: respBaseBody{
			Message: "Successfully Created",
		},
	}

	type createVMTestCase struct {
		testCaseBase

		baseImage string
		cores     int
		name      string
	}

	testCases := map[string]createVMTestCase{
		"create vm #1 is successful": {
			baseImage: "Mojave.img",
			cores:     4,
			testCaseBase: testCaseBase{
				token:                   "token1",
				statusCode:              http.StatusCreated,
				respBody:                respBody201,
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointCreate),
				expectedRequestBody:     getExpectedCreateRequestBody(testVMName, "Mojave.img", 4),
				expectedErr:             nil,
			},
			name: testVMName,
		},
		"create vm #2 is successful": {
			baseImage: "Catalina.img",
			cores:     4,
			testCaseBase: testCaseBase{
				token:                   "token2",
				statusCode:              http.StatusCreated,
				respBody:                respBody201,
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointCreate),
				expectedRequestBody:     getExpectedCreateRequestBody("myorkavm2", "Catalina.img", 4),
				expectedErr:             nil,
			},
			name: "myorkavm2",
		},
		"with invalid VM name": {
			baseImage: "Catalina.img",
			cores:     4,
			testCaseBase: testCaseBase{
				expectedRequestAttempts: 0,
				expectedErr:             ErrInvalidVMName,
			},
			name: "123myorkavm2",
		},
		"with orka returning error": {
			baseImage: "Catalina.img",
			cores:     4,
			testCaseBase: testCaseBase{
				token:      "token3",
				statusCode: http.StatusBadRequest,
				respBody: vmStatusRespBody{
					respBaseBody: respBaseBody{
						Errors: respBodyErrors{
							{
								Message: "Orka error message",
							},
						},
					},
				},
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointCreate),
				expectedRequestBody:     getExpectedCreateRequestBody(testVMName, "Catalina.img", 4),
				expectedErr: &HTTPError{
					Endpoint:   getVMEndpointRelURL(endpointCreate),
					StatusCode: http.StatusBadRequest,
					Message:    "",
					Errors:     []string{"Orka error message"},
				},
			},
			name: testVMName,
		},
		"create vm is successful after retry on StatusServiceUnavailable": {
			baseImage: "Mojave.img",
			cores:     4,
			testCaseBase: testCaseBase{
				token:                   "token1",
				statusCode:              http.StatusCreated,
				respBody:                respBody201,
				backoffSettings:         testBackoff,
				expectedRequestAttempts: 2,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointCreate),
				expectedRequestBody:     getExpectedCreateRequestBody(testVMName, "Mojave.img", 4),
				expectedErr:             nil,
			},
			name: testVMName,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			runTestCase(t, http.MethodPost, &tc.testCaseBase, func(c *client) error {
				return c.CreateVM(context.Background(), tc.name, tc.baseImage, tc.cores)
			})
		})
	}
}

func getExpectedCreateRequestBody(vmName string, baseImage string, cores int) createVMReqBody {
	return createVMReqBody{VMName: vmName, BaseImage: baseImage, Image: vmName, Cores: cores, VCores: cores}
}

func TestCreateVMReqBodyJSONSerialization(t *testing.T) {
	testCases := []createVMReqBody{
		{
			VMName:    "myorkavm1",
			BaseImage: "base-image-1",
			Image:     "image-1",
			Cores:     12,
			VCores:    24,
		},
		{
			VMName:    "myorkavm2",
			BaseImage: "base-image-2",
			Image:     "image-2",
			Cores:     6,
			VCores:    6,
		},
	}

	for _, tc := range testCases {
		json := serializeToJSON(t, tc)
		expectedJSON := fmt.Sprintf(
			`{"orka_vm_name":"%s","orka_base_image":"%s",`+
				`"orka_image":"%s","orka_cpu_core":%d,"vcpu_count":%d}`,
			tc.VMName,
			tc.BaseImage,
			tc.Image,
			tc.Cores,
			tc.VCores,
		)

		assert.Equal(t, json, expectedJSON)
	}
}
