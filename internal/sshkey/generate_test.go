package sshkey

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/ssh"
)

func TestGeneratingKey(t *testing.T) {
	priv, pub, err := Generate()
	assert.NoError(t, err)

	// ensure private key can be parsed correctly
	_, err = ssh.ParsePrivateKey(priv)
	assert.NoError(t, err)

	// verify format and length of public key
	assert.Regexp(t, regexp.MustCompile(`^ssh-rsa \S{372}\n`), string(pub))
}
