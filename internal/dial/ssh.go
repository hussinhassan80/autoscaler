// Copyright 2015 Gravitational, Inc.
// Modifications Copyright (c) 2015-2019 GitLab B.V.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dial

import (
	"context"
	"fmt"
	"net"
	"time"

	"golang.org/x/crypto/ssh"
)

const DefaultSSHTimeout = 10 * time.Second

// SSHWithDeadline performs a dial with the timeout specified in
// config.Timeout. If the dial is successful, config.Timeout is
// again used to setup a deadline for the SSH key exchange to be
// performed by. Canceling the context cancels both the dial and
// key exchange immediately.
// See https://github.com/golang/go/issues/15113
func SSHWithDeadline(ctx context.Context, network string, addr string, config *ssh.ClientConfig) (*ssh.Client, error) {
	dialer := &net.Dialer{
		Timeout: config.Timeout,
	}

	conn, err := dialer.DialContext(ctx, network, addr)
	if err != nil {
		return nil, fmt.Errorf("dialing with timeout: %w", err)
	}

	if config.Timeout > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, config.Timeout)
		defer cancel()
	}

	doneCh := make(chan error, 1)

	var client *ssh.Client
	go func() {
		c, chans, reqs, err := ssh.NewClientConn(conn, addr, config)
		if err != nil {
			doneCh <- fmt.Errorf("establishing connection: %w", err)
			return
		}

		client = ssh.NewClient(c, chans, reqs)
		close(doneCh)
	}()

	select {
	case err := <-doneCh:
		// if err != nil, conn.Close() has already been called by ssh.NewClientConn
		return client, err

	case <-ctx.Done():
		// explicitly close the dialed connection if the context has been canceled.
		conn.Close()

		return nil, fmt.Errorf("ssh handshake failed: %w", ctx.Err())
	}
}
